package com.techtalk.kafka_example.controller;

import com.techtalk.kafka_example.service.producer.KafkaProducerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    private final KafkaProducerService producerService;

    public KafkaController(KafkaProducerService producerService) {
        this.producerService = producerService;
    }

    @GetMapping("/send")
    public String sendMessage() {
        producerService.sendMessage("Hello, Kafka!");
        return "Message sent to Kafka.";
    }
}

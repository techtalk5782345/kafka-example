# Kafka Hello World Project

## Overview

Welcome to the Kafka Hello World project! This project is a simple demonstration to get you started with Apache Kafka, a powerful distributed event streaming platform. Learn how to send and receive messages using Kafka in no time!

## Getting Started

Follow these steps to set up and run the Kafka Hello World project:

### Prerequisites

1. Make sure you have Java installed on your machine.
2. Download and install Apache Kafka. [Apache Kafka Quickstart Guide](https://kafka.apache.org/quickstart)

### Project Setup

1. Clone this repository to your local machine.

   ```
   git clone https://gitlab.com/techtalk5782345/kafka-example
   ```

2. Navigate to the project directory:

   ```
   cd kafka-example
   ```

3. Build the project:

    ```
    mvn clean install
    ```

4. Run the application:

    ```
    mvn spring-boot:run
    ```

The application will start at `http://localhost:8080/kafka/send`. Open this URL in your browser, and you should be able to send message to specific kafka topic.